let collection = [];

// Write the queue functions below.
// To test your skills, try to use pure javascript coding and avoid using pop, push, slice, splice, etc.

function print() {
	// output all the elements of the queue
    return collection
}

function enqueue(element) {
	// add element to rear of queue
    this.elements[this.tail] = element;
    this.tail++;
}

function dequeue() {
	// remove element at front of queue
    const element = this.elements[this.head];
    delete this.elements[this.head];
    this.head++;
    return element;
}

function front() {
	// show element at the front
    return this.elements[this.head];
}

function size() {
    // show total number of elements
    return this.tail - this.head;
}

function isEmpty() {
    // outputs Boolean value describing whether queue is empty or not
    return this.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
